from rest_framework import serializers
from .models import Record
from .models import Tags
from .models import Author

class RecordSerializer(serializers.ModelSerializer):
    class Meta:
        model = Record
        fields = ('id', 'title', 'Author', 'descrition', 'date',)

class TagsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Tags
        fields = ('id', 'tag_title', 'descrition_tags',)

class AuthorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Author
        fields = ('id', 'Name', 'age', 'image', 'w', 'm',)