from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from .models import *
from .serializers import *
from rest_framework import viewsets
from django.shortcuts import get_object_or_404
# Create your views here.


class AuthorView(APIView):
    def get(self, request):
        authors = Author.objects.all()
        serializer = AuthorSerializer(authors, many=True)
        return Response({'authors': serializer.data})


    def post(self, request):
        authors = request.data.get('authors')
        serializer = AuthorSerializer(data=authors)
        if serializer.is_valid(raise_exception=True):
            authors_saved = serializer.save()
            return Response({"ок": "vse ok"})


class AuthorViewSet(viewsets.ModelViewSet):
    serializer_class = AuthorSerializer

    queryset = Author.objects.all()

class TagsView(APIView):
    def get(self, request):
        tagss = Tags.objects.all()
        serializer = TagsSerializer(tagss, many=True)
        return Response({'tagss': serializer.data})


    def post(self, request):
        tags = request.data.get('tagss')
        serializer = TagsSerializer(data=tags)
        if serializer.is_valid(raise_exception=True):
            tagss_saved = serializer.save()
            return Response({"ок": "vse ok"})


class TagsViewSet(viewsets.ModelViewSet):
    serializer_class = TagsSerializer

    queryset = Tags.objects.all()

class RecordView(APIView):
    def get(self, request):
        records = Record.objects.all()
        serializer = RecordSerializer(records, many=True)
        return Response({'records': serializer.data})


    def post(self, request):
        record = request.data.get('records')
        serializer = RecordSerializer(data=record)
        if serializer.is_valid(raise_exception=True):
            records_saved = serializer.save()
            return Response({"ок": "vse ok"})


class RecordViewSet(viewsets.ModelViewSet):
    serializer_class = RecordSerializer

    queryset = Record.objects.all()



