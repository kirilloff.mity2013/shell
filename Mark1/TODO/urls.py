from django.urls import path, include
from rest_framework.routers import DefaultRouter
from .views import RecordViewSet
from .views import TagsViewSet
from .views import AuthorViewSet

router = DefaultRouter()
router.register(r'Record', RecordViewSet, basename='record')
router.register(r'Tags', TagsViewSet, basename='tags')
router.register(r'Author', AuthorViewSet, basename='author')
urlpatterns = router.urls