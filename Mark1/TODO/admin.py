from django.contrib import admin
from .models import Author, Record, Tags
# Register your models here.
admin.site.register(Author)
admin.site.register(Record)
admin.site.register(Tags)